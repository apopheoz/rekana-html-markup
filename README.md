# Вёрстка страниц сайта компании Рекана

## Коротко

Этот репозиторий содержит свёрстанные шаблоны страниц сайта компании Рекана.

## Демонстрационная вёрстка страниц

**[Главная страница](https://apopheoz.ru/demo/rekana/index.html)**

**[Разводящая портфолио](https://apopheoz.ru/demo/rekana/portfolio.html)**

**[О нас](https://apopheoz.ru/demo/rekana/about.html)**

**[«Мысли вслух»](https://apopheoz.ru/demo/rekana/mysli.html)**

**[Разводящая «Мысли вслух»](https://apopheoz.ru/demo/rekana/mysli_razvod.html)**

**[Внутренняя страница раздела «Портфолио»](https://apopheoz.ru/demo/rekana/portfolio_item.html)**

**[Внутренняя страница раздела «Мысли вслух»](https://apopheoz.ru/demo/rekana/mysli_inside.html)**

**[Контакты](https://apopheoz.ru/demo/rekana/contacts.html)**

**[Блог. Внутренняя](https://apopheoz.ru/demo/rekana/blog_inside.html)**

**[Блог. Разводящая](https://apopheoz.ru/demo/rekana/blog_razvod.html)**

**[Магазин. Разводящая](https://apopheoz.ru/demo/rekana/magazin_razvod.html)**

**[Магазин. Внутренняя](https://apopheoz.ru/demo/rekana/magazin_inside.html)**

**[Маркетинг продаж](https://apopheoz.ru/demo/rekana/mark_prodaj.html)**

**[Видео. Разводящая](https://apopheoz.ru/demo/rekana/video_razvod.html)**

**[Видео. Внутренняя](https://apopheoz.ru/demo/rekana/video_inside.html)**

## Ссылки

**[Рекана](http://rekana.ru)** - Маркетинговое агентство Рекана;

**[apopheoz.ru](https://apopheoz.ru)** - лаборатория веб-дизайна «Апофеозъ»;

## Контактная информация

Отзывы и предложения: http://apopheoz.ru/feedback.html
