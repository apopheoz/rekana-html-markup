#!/bin/sh

rm -f *.win1251
for f in ./*.html; do
	cat $f | sed 's/charset\=\"utf-8\"/charset\=\"windows-1251\"/' | iconv -f UTF-8 -t CP1251 -c -o $f.win1251;
done
